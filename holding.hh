#ifndef HOLDING_HH
#define HOLDING_HH

#include <ostream>

template <unsigned int _acc, unsigned int _hs, unsigned int _exo>
class Company {
public:
	static constexpr unsigned int acc = _acc;
	static constexpr unsigned int hs  = _hs;
	static constexpr unsigned int exo = _exo;
};

// Wymagane deklaracje typów
typedef Company<1,0,0> Accountancy;
typedef Company<0,1,0> Hunting_shop;
typedef Company<0,0,1> Exchange_office;

// Funkcje pomocnicze pozwalające na wykonanie
// działań zgodnie z treścią zadania:

// Bezpieczne odejmowanie
static constexpr inline unsigned int _subtract(unsigned int x1, unsigned int x2) {
    return (x2 > x1) ? 0 : x1 - x2;
}

// Bezpieczne dzielenie
static constexpr inline unsigned int _divide(unsigned int x1, unsigned int x2) {
    return (x2 == 0) ? 0 : x1 / x2;
}

template<class C1, class C2>
struct add_comp {
	typedef Company <
		C1::acc + C2::acc,
		C1::hs  + C2::hs,
		C1::exo + C2::exo
	> type;
};

template<class C1, class C2>
struct remove_comp {
	typedef Company <
		_subtract(C1::acc, C2::acc),
		_subtract(C1::hs, C2::hs),
		_subtract(C1::exo, C2::exo)
	> type;
};

template<class C, unsigned int n>
struct multiply_comp {
	typedef Company <
		C::acc * n,
		C::hs  * n,
		C::exo * n
	> type;
};

template<class C, unsigned int n>
struct split_comp {
	typedef Company <
		_divide(C::acc, n),
		_divide(C::hs, n),
		_divide(C::exo, n)
	> type;
};

template<class C>
struct additive_expand_comp {
	typedef typename add_comp<C, Company<1,1,1>>::type type;
};

template<class C>
struct additive_rollup_comp {
	typedef typename remove_comp<C, Company<1,1,1>>::type type;
};

template<class C>
class Group {
	template<class C2>
	friend std::ostream & operator<<(std::ostream & os, Group<C2> const & grupa);

public:
	typedef C company_type;
	static company_type company;

	Group() = default;
	Group(unsigned int k);
	Group(Group<C> const &);

	unsigned int get_size() const;

	void set_acc_val(unsigned int i);
	void set_hs_val(unsigned int i);
	void set_exo_val(unsigned int i);

	unsigned int get_acc_val() const;
	unsigned int get_hs_val() const;
	unsigned int get_exo_val() const;

	unsigned int get_value() const;

	Group<C> operator+(Group<C> const &) const;
	Group<C> operator-(Group<C> const &) const;
	Group<C>& operator+=(Group<C> const &);
	Group<C>& operator-=(Group<C> const &);

	Group<C> operator*(unsigned int) const;
	Group<C> operator/(unsigned int) const;
	Group<C>& operator*=(unsigned int);
	Group<C>& operator/=(unsigned int);

	bool operator<=(Group<C> const &) const;
	template<class C2>
	bool operator<=(Group<C2> const &) const;
	template<class C2>
	bool operator==(Group<C2> const &) const;
	template<class C2>
	bool operator!=(Group<C2> const &) const;
	template<class C2>
	bool operator<(Group<C2> const &) const;
	template<class C2>
	bool operator>(Group<C2> const &) const;
	template<class C2>
	bool operator>=(Group<C2> const &) const;

private:
	unsigned int companies_count = 1;
	unsigned int acc_val = 15;
	unsigned int hs_val = 150;
	unsigned int exo_val = 50;
};

// Implementacja metod klasy Group
template<class C>
Group<C>::Group(unsigned int k) :
	companies_count(k)
{ }

template<class C>
Group<C>::Group(Group<C> const & other) :
	companies_count(other.companies_count),
	acc_val(other.acc_val),
	hs_val(other.hs_val),
	exo_val(other.exo_val)
{ }

template<class C>
unsigned int Group<C>::get_size() const {
	return companies_count;
}

template<class C>
void Group<C>::set_acc_val(unsigned int i) {
	acc_val = i;
}

template<class C>
void Group<C>::set_hs_val(unsigned int i) {
	hs_val = i;
}

template<class C>
void Group<C>::set_exo_val(unsigned int i) {
	exo_val = i;
}

template<class C>
unsigned int Group<C>::get_acc_val() const {
	return acc_val;
}

template<class C>
unsigned int Group<C>::get_hs_val() const {
	return hs_val;
}

template<class C>
unsigned int Group<C>::get_exo_val() const {
	return exo_val;
}

template<class C>
unsigned int Group<C>::get_value() const {
	return companies_count * (C::acc * acc_val + C::hs * hs_val + C::exo * exo_val);
}

// Operatory += oraz -= posłużą do zaimplementowania operatorów + i -
template<class C>
Group<C>& Group<C>::operator+=(Group<C> const & op) {
	acc_val = _divide((C::acc * companies_count * acc_val + C::acc * op.companies_count * op.acc_val),
	                  C::acc * (companies_count + op.companies_count));
	hs_val  = _divide((C::hs  * companies_count * hs_val  + C::hs  * op.companies_count * op.hs_val ),
	                  C::hs  * (companies_count + op.companies_count));
	exo_val = _divide((C::exo * companies_count * exo_val + C::exo * op.companies_count * op.exo_val),
	                  C::exo * (companies_count + op.companies_count));
	companies_count += op.companies_count;
	
	return *this;
}

template<class C>
Group<C>& Group<C>::operator-=(Group<C> const & op) {
	acc_val = _divide(_subtract(C::acc * companies_count * acc_val, C::acc * op.companies_count * op.acc_val),
	                  C::acc * _subtract(companies_count, op.companies_count));
	hs_val  = _divide(_subtract(C::hs  * companies_count * hs_val , C::hs  * op.companies_count * op.hs_val ),
	                  C::hs  * _subtract(companies_count, op.companies_count));
	exo_val = _divide(_subtract(C::exo * companies_count * exo_val, C::exo * op.companies_count * op.exo_val),
	                  C::exo * _subtract(companies_count, op.companies_count));
	companies_count = _subtract(companies_count, op.companies_count);	
	
	return *this;
}

template<class C>
Group<C> Group<C>::operator+(Group<C> const & op) const {
	return Group<C>(*this) += op;
}

template<class C>
Group<C> Group<C>::operator-(Group<C> const & op) const {
	return Group<C>(*this) -= op;
}

template<class C>
Group<C> Group<C>::operator*(unsigned int op) const {
	Group<C> result(companies_count * op);
	result.acc_val = _divide(acc_val, op);
	result.hs_val  = _divide(hs_val , op);
	result.exo_val = _divide(exo_val, op);
	return result;
}

template<class C>
Group<C> Group<C>::operator/(unsigned int op) const {
	Group<C> result(_divide(companies_count, op));
	result.acc_val = acc_val * op;
	result.hs_val  = hs_val  * op;
	result.exo_val = exo_val * op;
	return result;
}

template<class C>
Group<C>& Group<C>::operator*=(unsigned int op) {
	*this = *this * op;
	return *this;
}

template<class C>
Group<C>& Group<C>::operator/=(unsigned int op) {
	*this = *this / op;
	return *this;
}

// Wszystkie operatory zostaną zaimplelentowane przy pomocy <=

// Zgodnie z treścią odpowiedzi na forum jeśli porównywane grupy
// mają tę samą firmę bazową, kryterium porównawczym jest stosunek 
// ilości tych firm.
template<class C>
bool Group<C>::operator<=(Group<C> const & op) const {
	return this->get_size() <= op.get_size();
}

// W przeciwnym razie porównujemy wartość exo i hs.
template<class C>
template<class C2>
bool Group<C>::operator<=(Group<C2> const & op) const {
	return (((C::exo * this->get_size()) <= (C2::exo * op.get_size())) &&
	        ((C::hs  * this->get_size()) <= (C2::hs  * op.get_size())));
}

template<class C>
template<class C2>
bool Group<C>::operator==(Group<C2> const & op) const {
	return (*this <= op) && (op <= *this);
}

template<class C>
template<class C2>
bool Group<C>::operator!=(Group<C2> const & op) const {
	return !(*this == op);
}

template<class C>
template<class C2>
bool Group<C>::operator<(Group<C2> const & op) const {
	return (*this <= op) && (*this != op);
}

template<class C>
template<class C2>
bool Group<C>::operator>(Group<C2> const & op) const {
	return op < *this;
}

template<class C>
template<class C2>
bool Group<C>::operator>=(Group<C2> const & op) const {
	return op <= *this;
}
// Koniec implementacj metod klasy Group.

// Operator << wypisujący na strumień opis grupy.
template<class C>
std::ostream & operator<<(std::ostream &os, Group<C> const &grupa) {
	os << "Number of companies: " << grupa.get_size()
	   << "; Value: " << grupa.get_value()
	   << std::endl
	   << "Accountancies value: "      << grupa.get_acc_val()
	   << ", Hunting shops value: "    << grupa.get_hs_val()
	   << ", Exchange offices value: " << grupa.get_exo_val()
	   << std::endl
	   << "Accountancies: "      << C::acc
	   << ", Hunting shops: "    << C::hs
	   << ", Exchange offices: " << C::exo
	   << std::endl;
	return os;
}

template<class C>
Group<typename additive_expand_comp<C>::type> const
additive_expand_group(Group<C> const &s1) {
	Group<typename additive_expand_comp<C>::type> result(s1.get_size());
	result.set_acc_val(s1.get_acc_val());
	result.set_hs_val(s1.get_hs_val());
	result.set_exo_val(s1.get_exo_val());
	return result;
}

template<class C>
Group<typename multiply_comp<C, 10>::type> const
multiplicative_expand_group(Group<C> const &s1) {
	Group<typename multiply_comp<C, 10>::type> result(s1.get_size());
	result.set_acc_val(s1.get_acc_val());
	result.set_hs_val(s1.get_hs_val());
	result.set_exo_val(s1.get_exo_val());
	return result;
}

template<class C>
Group<typename additive_rollup_comp<C>::type> const
additive_rollup_group(Group<C> const &s1) {
	Group<typename additive_rollup_comp<C>::type> result(s1.get_size());
	result.set_acc_val(s1.get_acc_val());
	result.set_hs_val(s1.get_hs_val());
	result.set_exo_val(s1.get_exo_val());
	return result;
}

template<class C>
Group<typename split_comp<C, 10>::type> const
multiplicative_rollup_group(Group<C> const &s1) {
	Group<typename split_comp<C, 10>::type> result(s1.get_size());
	result.set_acc_val(s1.get_acc_val());
	result.set_hs_val(s1.get_hs_val());
	result.set_exo_val(s1.get_exo_val());
	return result;
}

template<class C1, class C2, class C3>
bool
solve_auction(Group<C1> const &g1, Group<C2> const &g2, Group<C3> const &g3) {
	return (g1 > g2 && g1 > g3) || (g2 > g1 && g2 > g3) || (g3 > g2 && g3 > g1);
}

#endif
